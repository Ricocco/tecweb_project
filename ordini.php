<?php include("check_login.php");?>
<div id="corpo">
  <header>
    <h1>Ordini</h1>
  </header>
<?php
  $query = "SELECT * FROM ordini WHERE ordini.id_stato != 1 AND ordini.id_stato != 6 GROUP BY ordini.id_utente ORDER BY ordini.id_stato DESC";
  $res = $conn->query($query);
  while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)){
    $query_name = "SELECT * FROM utenti WHERE email ='".$row['id_utente']."'";
    $res_name = $conn->query($query_name);
    $row_name = mysqli_fetch_array($res_name, MYSQLI_ASSOC);
    echo "<div class ='ordine'>";
    echo "<h2>".$row_name['nome']." ".$row_name['cognome']."</h2>";
    echo "<h3>".$row['id_utente']."</h3>";
    // stampare il nome dell'utente

    $query = "SELECT * FROM ordini WHERE ordini.id_stato != 1 AND ordini.id_stato != 6 AND ordini.id_utente = '".$row['id_utente']."'";
    $res_prodotti = $conn->query($query);
    ?>
    <div>
    <table>
      <tr>
        <th>Prodotto</th>
        <th>Quantità</th>
      </tr>
<?php
    $tot = 0;
    while ($row_prodotti = mysqli_fetch_array($res_prodotti, MYSQLI_ASSOC)){
      $indirizzo = $row_prodotti['indirizzo'];
      echo "<tr>";
      $query_prod_name = "SELECT * FROM prodotto WHERE prodotto.id_prodotto ='".$row_prodotti['id_prodotto']."'";
      $res_prod_name = $conn->query($query_prod_name);
      $row_prod_name = mysqli_fetch_array($res_prod_name, MYSQLI_ASSOC);
      $tot += $row_prod_name['prezzo']*$row_prodotti['quantita'];
      echo "<td>".$row_prod_name['nome']."</td>";
      echo "<td>".$row_prodotti['quantita']."</td>";
      echo "</tr>";
    }
    echo "<tr><td class='line'>Prezzo totale: ".$tot."&euro;</td></tr>";
    echo "<tr><td>Indirizzo di consegna: ".$indirizzo."</td></tr>";
    echo "<tr><td class='line'>Stato:";
    $query_state_name = "SELECT * FROM stato WHERE stato.id_stato ='".$row['id_stato']."'";
    $res_state_name = $conn->query($query_state_name);
    $row_state_name = mysqli_fetch_array($res_state_name, MYSQLI_ASSOC);
    echo $row_state_name['nome'];
    echo "</td></tr>";
    echo "</table>";
    echo "</div>";
    echo "<input class='statoSuccessivo' type='submit' value='".$row_state_name['successivo']."'>";
?>

</div>
<?php
  }
?>
</div>
