<div id="corpo">
  <header>
    <h1>Carrello personale</h1>
  </header>
<?php
include("db_Con.php");
include("check_login.php");
$query_ordine_nc = "SELECT * FROM ordini WHERE ordini.id_stato = 2 AND ordini.id_utente = '".$_SESSION['email']."'";
$result_ordine_nc = $conn->query($query_ordine_nc);
$query_c = "SELECT * FROM ordini WHERE ordini.id_stato != 2 AND ordini.id_stato != 1 AND ordini.id_stato != 6 AND ordini.id_utente = '".$_SESSION['email']."'";
$result_c = $conn->query($query_c);

if(($result_c->num_rows > 0) || ($result_ordine_nc->num_rows > 0)){
  echo "<div class='cart'>";
  echo "<h2>Ordine in corso</h2>";
  echo "<table><tr><td>";
  echo "<h3>Stato : ";
  $isCart = false;
  $isMod = false;
  if($result_c->num_rows > 0){
    $result = $result_c;
  }else{
    $result = $result_ordine_nc;
    $isMod = true;

  }
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
  $indirizzo = $row['indirizzo'];
  $query_state_name = "SELECT * FROM stato WHERE stato.id_stato ='".$row['id_stato']."'";
  $res_state_name = $conn->query($query_state_name);
  $row_state_name = mysqli_fetch_array($res_state_name, MYSQLI_ASSOC);
  echo $row_state_name['nome']."</h3></td>";
  if($isMod){
        echo "<td><img class='iconEliminaOrdine' src='img/cross.png' alt='Elimina'></td></tr></table>";
  }
  include("carrello_base.php");
  echo "<h3 class='info'>Indirizzo di consegna: ".$indirizzo."</h3>";
  echo "</div>";
}
//parte del carrello
$space = false;
$query = "SELECT * FROM ordini WHERE ordini.id_stato = 1 AND ordini.id_utente = '".$_SESSION['email']."'";
$result = $conn->query($query);
$isMod = true;
$isCart = true;
echo "<h2>Carrello:</h2>";
echo "<hr align=center size='1' width='90%' color=black noshade>";
echo "<div class='cart'>";
if($result->num_rows == 0){
  $space = true;
  echo "<h3 class='info'>Il carrello è vuoto!</h3>";
}else{
  //---------------Carrello---------------------
  $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

  include("carrello_base.php");


  //-------------------------------------------------

  //per non dare la possibilità di fare due ordini
  $query = "SELECT * FROM ordini WHERE ordini.id_stato != 1 AND ordini.id_stato != 2 AND ordini.id_stato != 6 AND ordini.id_utente = '".$_SESSION['email']."'";
  $result = $conn->query($query);
  if($result->num_rows > 0){
    echo "<p>Non puoi effettuare un altro ordine finche il precendente non è stato completato</p>";
  }else{
    $query = "SELECT * FROM ordini WHERE ordini.id_stato = 2 AND ordini.id_utente = '".$_SESSION['email']."'";
    $result = $conn->query($query);
    if($result->num_rows){
      echo "<input class='butt' type='submit' value='Aggiungi' id='aggiungiOrdine'>";
    }else{
      echo "<input class ='butt' type='submit' value='Procedi' id='avanza'>";
    }

  }
}
if($space){
?>
</div>
<div id="space">
</div>
<?php } ?>
</div>
</div>
