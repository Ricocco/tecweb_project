<?php
include("db_Con.php");
include("checkAdmin.php");
$query_info = "SELECT * FROM utenti WHERE utenti.fornitore =1";
$res = $conn->query($query_info);
$row = mysqli_fetch_array($res, MYSQLI_ASSOC);

if(isset($_POST['nome'])){
  $nome = $_POST['nome'];
  $cognome = $_POST['cognome'];
  $cognome = $_POST['cognome'];
  $via = $_POST['via'];
  $numero = $_POST['numero'];
  $cap = $_POST['cap'];
  $provincia = $_POST['prov'];
  $cel = $_POST['cel'];

  $query = "UPDATE `utenti` SET `nome`='".$nome."',`cognome`='".$cognome."', `numeroCivico`={$numero},`cap`={$cap} ,`via`='".$via."',`provincia`='".$provincia."',`cell`='".$cel."' WHERE utenti.fornitore = 1";
  $res = $conn->query($query);
  header("location: home.php?p=3");
}

 ?>
 <div id="corpo">
   <header>
     <h1>Modifica Info</h1>
   </header>
   <form id="formImpostazioni" action="#" method="post">
     <fieldset>
       <legend>Nominativo</legend>
       <label for="nome">Nome</label>
       <?php echo '<input type="text" name="nome" value="'.$row['nome'].'" required>'; ?>
       <label for="cognome">Cognome</label>
       <?php echo '<input type="text" name="cognome" value="'.$row['cognome'].'" required>'; ?>
     </fieldset>
     <fieldset>
       <legend>Indirizzo attività</legend>
       <label for="via">Via</label>
       <?php echo '<input type="text" name="via" value="'.$row['via'].'" required>'; ?>
       <label for="numero">Numero civico</label>
       <?php echo '<input type="number" step="1" min="1" name="numero" value="'.$row['numeroCivico'].'"required>'; ?>
       <label for="cap">CAP</label>
       <?php echo '<input type="number" step="1" min="10000" name="cap" value="'.$row['cap'].'" required>'; ?>
       <label for="prov">Provincia</label>
       <?php echo '<input type="text" name="prov" value="'.$row['provincia'].'" required>'; ?>
     </fieldset>
     <fieldset>
       <legend>Contatti</legend>
       <label for="cel">Telefono</label>
       <?php echo '<input type="text" name="cel" value="'.$row['cell'].'" required>'; ?>
       <!--
       <label for="email">E-mail</label>
       <?php echo '<input type="email" name="email" value="'.$row['email'].'" required>'; ?>
        -->
     </fieldset>
     <input type="submit" value="Modifica">
   </form>
 </div>
