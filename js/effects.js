$(document).ready(function(){
  $("#menu").mouseover(function(){
      $("#menu").css("background-color", "gray");
      $("#menu").css("text", "black");
  });

  $("#menu").mouseout(function(){
    $("#menu").css("background-color", "black");
    $("#menu").css("text", "white");
  });

  $("#menu").click(function(){
    if(window.location.href !== "home.php?p=1"){
        window.location.href = "home.php?p=1";
    }
  });

  $("#avanza").click(function(){
    window.location.href = "home.php?p=5";
  });

  $("#ordini").mouseover(function(){
      $("#ordini").css("background-color", "gray");
      $("#ordini").css("text", "black");
  });

  $("#ordini").mouseout(function(){
    $("#ordini").css("background-color", "black");
    $("#ordini").css("text", "white");
  });

  $("#insIndirizzo").click(function(){
    $("#inserisci_indirizzo").slideToggle();
  });

  $("#ordini").click(function(){
      window.location.href = "home.php?p=7";
  });

  $("#recensioni").mouseover(function(){
      $("#recensioni").css("background-color", "gray");
      $("#recensioni").css("text", "black");
  });

  $("#recensioni").mouseout(function(){
    $("#recensioni").css("background-color", "black");
    $("#recensioni").css("text", "white");
  });

  $("#recensioni").click(function(){
    if(window.location.href !== "home.php?p=2"){
        window.location.href = "home.php?p=2";
    }
  });

  $("#info").mouseover(function(){
      $("#info").css("background-color", "gray");
      $("#info").css("text", "black");
  });

  $("#info").mouseout(function(){
    $("#info").css("background-color", "black");
    $("#info").css("text", "white");
  });

  $("#info").click(function(){
    if(window.location.href !== "home.php?p=3"){
        window.location.href = "home.php?p=3";
    }
  });

  $(".formRecensione").hide();

  $("#WriteRec").click(function(){
    $(".formRecensione").slideToggle();
  })

  $(".carrello").click(function(){
      window.location.href = "home.php?p=4";
  })

  $("#logo").click(function(){
      window.location.href = "home.php";
  })

  $("#aggiungiOrdine").click(function(){
    window.location.href = "conferma_ordine.php";
  })

  $("#pagamento").click(function(){
    window.location.href = "home.php?p=6";
  })

  $(".impostazioni").click(function(){
    window.location.href = "home.php?p=9";
  });

  $(".statoSuccessivo").click(function(){
    nome = ($(this).prev().parent().children("h3").text());
    $.ajax({
      type: "POST",
      url: "avanza_stato.php",
      data: "nome="+nome,
      dataType: "html",
      success: function(msg)
      {
        location.reload();
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare...");
      }
    });
  });

  var width = $(".categoryBox").width();
  var height = $(".categoryBox").height();
  $(".imgCategoryBox").css("width", width);
  $(".imgCategoryBox").css("height", height);

});
