$(document).ready(function(){
  $(".aggiungi").click(function(){
    //associo variabili
    var quantita = $(this).parent().prev().prev().text();
    var prodotto = $(this).parent().prev().prev().prev().text();
    var id_res = $(this).parent().prev().prev().attr("id");
  //chiamata ajax
    $.ajax({

     //imposto il tipo di invio dati (GET O POST)
      type: "POST",

      //Dove devo inviare i dati recuperati dal form?
      url: "modifica_quantita.php",

      //Quali dati devo inviare?
      data: "quantita=" + quantita + "&val=+1" + "&prodotto="+prodotto+"&stato=1",
      dataType: "html",

      //Inizio visualizzazione errori
      success: function(msg)
      {
        $("#"+id_res).html(msg);
         location.reload();
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare..."); //sempre meglio impostare una callback in caso di fallimento
      }
    });
  });

  $(".aggiungiOrdine").click(function(){
    //associo variabili
    var quantita = $(this).parent().prev().prev().text();
    var prodotto = $(this).parent().prev().prev().prev().text();
    var id_res = $(this).parent().prev().prev().attr("id");
  //chiamata ajax
    $.ajax({

     //imposto il tipo di invio dati (GET O POST)
      type: "POST",

      //Dove devo inviare i dati recuperati dal form?
      url: "modifica_quantita.php",

      //Quali dati devo inviare?
      data: "quantita=" + quantita + "&val=+1" + "&prodotto="+prodotto+"&stato=2",
      dataType: "html",

      //Inizio visualizzazione errori
      success: function(msg)
      {
        $("#"+id_res).html(msg);
         location.reload();
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare..."); //sempre meglio impostare una callback in caso di fallimento
      }
    });
  });

  $(".rimuovi").click(function(){
    //associo variabili
    var quantita = $(this).parent().prev().prev().text();
    var prodotto = $(this).parent().prev().prev().prev().text();
    var id_res = $(this).prev().prev().attr("id");
  //chiamata ajax
    $.ajax({

     //imposto il tipo di invio dati (GET O POST)
      type: "POST",

      //Dove devo inviare i dati recuperati dal form?
      url: "modifica_quantita.php",

      //Quali dati devo inviare?
      data: "quantita=" + quantita + "&val=-1" + "&prodotto="+prodotto+"&stato=1",
      dataType: "html",

      //Inizio visualizzazione errori
      success: function(msg)
      {
        $("#"+id_res).html(msg);
        location.reload();
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare..."); //sempre meglio impostare una callback in caso di fallimento
      }
    });
  });

  $(".rimuoviOrdine").click(function(){
    //associo variabili
    var quantita = $(this).parent().prev().prev().text();
    var prodotto = $(this).parent().prev().prev().prev().text();
    var id_res = $(this).prev().prev().attr("id");
  //chiamata ajax
    $.ajax({

     //imposto il tipo di invio dati (GET O POST)
      type: "POST",

      //Dove devo inviare i dati recuperati dal form?
      url: "modifica_quantita.php",

      //Quali dati devo inviare?
      data: "quantita=" + quantita + "&val=-1" + "&prodotto="+prodotto+"&stato=2",
      dataType: "html",

      //Inizio visualizzazione errori
      success: function(msg)
      {
        $("#"+id_res).html(msg);
        location.reload();
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare..."); //sempre meglio impostare una callback in caso di fallimento
      }
    });
  });


  $(".iconAggiungiCarrello").click(function(){
      var div = $(this);
      div.animate({height: '30pt', opacity: '0.4'}, "fast");
      div.animate({width: '30pt', opacity: '0.8'}, "fast");
      div.animate({height: '20pt', opacity: '0.4'}, "fast");
      div.animate({width: '20pt', opacity: '1'}, "fast");
      $(".carrello").animate({
          height: '+=10px',
          width: '+=10px'
      });
      $(".carrello").animate({
          height: '-=10px',
          width: '-=10px'
      });
     var p = $(this).parent().prev().text();
     var q = document.getElementById("qt"+p).value;
     $.ajax({

      //imposto il tipo di invio dati (GET O POST)
       type: "POST",

       //Dove devo inviare i dati recuperati dal form?
       url: "aggiungi_carrello.php",

       //Quali dati devo inviare?
       data: "p=" + p + "&q="+q,
       dataType: "html",

       //Inizio visualizzazione errori
       success: function(msg)
       {
          $("#numeroOggetti").html(msg);
       },
       error: function()
       {
         alert("Chiamata fallita, si prega di riprovare..."); //sempre meglio impostare una callback in caso di fallimento
       }
     });
  })

});
