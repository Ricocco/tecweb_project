function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace(
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;
	}
	return vars;
}

$(document).ready(function(){
  error = $_GET('error');
  console.log(error);
  if (error == 2){
    $("p#errore").html("Errore - e-mail già presente!");
    setTimeout(function(){$("p#errore").fadeOut()}, 5000);
  }else if(error == 1){
      $("p#errore").html("Errore - le password inserite non corrispondono!");
      setTimeout(function(){$("p#errore").fadeOut()}, 5000);
  }
});
