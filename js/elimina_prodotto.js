$(document).ready(function(){
  $(".iconElimina").click(function(){
    var i = $(this).parent().prev().text();
    $.ajax({
      type: "POST",
      url: "elimina_prodotto.php",
      data: "i="+i,
      dataType: "html",
      success: function(msg)
      {
        location.reload();
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare...");
      }
    });
  });

  $(".iconEliminaOrdine").click(function(){
    window.location.href = "elimina_ordine.php";
  });
});
