$(document).ready(function(){
  var indirizzo = $(".indirizzo").first().children().html();
  $(".indirizzo").first().css("background-color", "#ea2c39");

  $(".indirizzo").click(function(){
    $(".indirizzo").css("background-color", "white");
      indirizzo = $(this).children().html();
      $(this).css("background-color", "#ea2c39");
  });

  $("#completaOrdine").click(function(){
    $.ajax({
     //imposto il tipo di invio dati (GET O POST)
      type: "POST",

      //Dove devo inviare i dati recuperati dal form?
      url: "conferma_ordine.php",

      //Quali dati devo inviare?
      data: "indirizzo="+indirizzo,
      dataType: "html",

      success: function()
      {
         window.location.href = "home.php?p=4";
      },
      error: function()
      {
        alert("Chiamata fallita, si prega di riprovare..."); //sempre meglio impostare una callback in caso di fallimento
      }
    });
  });

});
