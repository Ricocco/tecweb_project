<?php
session_start();
include("db_Con.php");

if(!empty($_SESSION['email'])){
  header("location: home.php");
}

if(isset($_POST['email'])){
  $username = $_POST['email'];
  $password = $_POST['psw'];
  $query = "SELECT * FROM utenti WHERE utenti.email = '$username' AND utenti.psw = SHA1('".$password."')" ;
  $result = $conn->query($query);
  if($result->num_rows==0){
      header("location: index.php?error=1");
  }else{
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $_SESSION['admin'] = $row['fornitore'];
    $_SESSION['email'] = $_POST['email'];
    header("location: home.php");
  }
}

?>

<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>uniEat</title>
  <link rel="stylesheet" type="text/css" title="stylesheet" href="style/style_user.css">
  <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
</head>
<body class="notLogged">
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/loginError.js"></script>
	<section>
		<header>
			<h1>Accedi a uniEat</h1>
		</header>
		<form name="form_login" method="post" action="#">
      <fieldset>
				<legend><span>1</span>Inserisci i dati</legend>
        <p id="erroreLogin">errore e-mail o password non corretti!</p>

				<ul>
					<li><label for="email">E-mail</label></li>
					<li><input id="email" type="email" name="email" required/></li>
					<li><label for="psw">Password</label></li>
					<li><input id="psw" type="password" name="psw" required/></li>
				</ul>
			</fieldset>
      <a href="registrazione.php">altrimenti, Registrati!</a><br>
      <input id= "accedi" type="submit" name="Submit" value="Accedi" />
      <input id="reset" type="reset" value="Reset" /><label for="reset">Reset</label>
  </form>
</section>
</body>
</html>
