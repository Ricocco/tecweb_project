<div id="corpo">
  <header>
    <h1>Info</h1>
  </header>
  <div id="orario">
    <h2>Orario di apertura</h2>
    <table id = "orarioApertura">
      <tbody>
        <tr>
          <th>Giorno</th>
          <th>Apertura</th>
          <th>Chiusura</th>
        </tr>
        <?php
          $query = "SELECT * FROM orario ORDER by orario.index";
          $result = $conn->query($query);
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo "<tr>";
            echo "<td>".$row['giorno']."</td>";
            if($row['apertura'] == NULL){
              echo "<td> chiuso </td>";
            }else{
              echo "<td>".$row['apertura']."</td>";
              echo "<td>".$row['chiusura']."</td>";
            }
            echo "</tr>";
          }
        ?>
      </tbody>
    </table>
  </div>
  <div id = "contatti">
    <h2>Contatti</h2>
    <?php
      $query = "SELECT * FROM utenti WHERE fornitore = 1";
      $result = $conn->query($query);
      $res = mysqli_fetch_array($result, MYSQLI_ASSOC);
      echo "<p> Indirizzo: via " . $res['via'] . " " .$res['numeroCivico']. ", " .$res['cap']. " (". $res['provincia'].") </p>";
      echo "<p> Telefono: ". $res['cell']. ".</p>";
      echo "<p> E-mail: ". $res['email']. "</p>";
    ?>
  </div>
</div>
