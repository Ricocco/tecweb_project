<?php
session_start();
include("db_Con.php");
  if($_SESSION['email']==NULL){
      header("location: index.php");
  }
?>


<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>uniEat</title>
    <?php
      if($_SESSION['admin'] === "0"){
       echo "<link rel='stylesheet' type='text/css' title='stylesheet2' href='style/style_user.css'>";
     }else{
       echo "<link rel='stylesheet' type='text/css' title='stylesheet' href='style/style_admin.css'>";
     }
     ?>
    <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/logout.js"></script>
    <script src="js/effects.js"></script>
    <script src="js/modifica_carrello.js"></script>
    <script src="js/elimina_prodotto.js"></script>
    <?php
    if($_SESSION['admin'] == "1"){
      echo "<script src='js/aggiungi_prodotto.js'></script>";
    }else {
      echo "<script src='js/scroll.js'></script>";
    }
    ?>
  </head>
  <body class="home">
    <header id="homeHeader">
      <figure>
          <img id="logo" src="img/logo.png" alt="UniEAT" >
      </figure>
      <div id="siteDescription">
        <h2>Un modo per mangiare, senza muoverti dal campus!</h2>
      </div>
        <div id="name">
          <?php
          if($_SESSION['email']!=NULL){
            $query_name = "SELECT * FROM utenti WHERE utenti.email = '".$_SESSION['email']."'";
            $result = $conn->query($query_name);
            foreach($result as $val )
                  $_SESSION["admin"] = $val['fornitore'];
                  $_SESSION['page'] = "home";
          }
          echo "<table><tr>";
          if($_SESSION['admin'] == "0"){
          ?>
              <td>
                <figure>
                  <img class="carrello" src="img/carrello.png" alt="Carrello" >
                </figure>
              </td>
          <?php } else{ ?>
            <td>
              <figure>
                <img class="impostazioni" src="img/impostazioni.png" alt="Impostazioni" >
              </figure>
            </td>
          <?php } ?>
            <td>
          <?php
              echo "<p id= 'username'>Ciao, ".$val['nome']."</p>";
          ?>
            </td>
          </tr>
            <tr>
              <?php
                $query_num_oggetti = "SELECT * FROM ordini WHERE id_utente = '".$_SESSION['email']."' AND id_stato = 1";
                $res = $conn->query($query_num_oggetti);
                $num = 0;
                while($row = mysqli_fetch_array($res, MYSQLI_ASSOC)){
                    $num += $row['quantita'];
                }
                if($_SESSION['admin'] == 0){
               ?>

              <td id="numeroOggetti"><?php echo $num ?></td>
            <?php } else{
              echo "<td></td>";
            }?>
              <td><a href="logout.php" id="logout">Logout</a></td>
            </tr>
          </table>
        </div>
        <nav>
             <div id="menu">Menù</div>
             <div id="recensioni">Recensioni</div>
             <div id="info">Info</div>


         <?php
         if($_SESSION['admin'] == 1){
           echo "<div id='ordini'>Ordini</div>";
         }
         ?>
       </nav>
    </header>

       <?php
            if(!isset($_GET['p']) ||$_GET["p"] === "1"){
                include("home_body.php");
            }else if ($_GET["p"] === "2"){
              include("recensioni.php");
            }else if($_GET["p"] === "3"){
                include("info.php");
            }else if($_GET["p"] === "4"){
              if(isset($_GET['error'])){
                if($_GET['error']==1){
                  echo "<p>Spiacenti, non puoi effettuare un altro ordine fino a quando il precendente non è stato completato!";
                }
              }
                include("carrello.php");
            }else if($_GET["p"] === "5"){
                include("indirizzo.php");
            }else if($_GET["p"] === "6"){
                include("pagamento.php");
            }else if($_GET["p"] === "7"){
              if($_SESSION['admin']==1){
                include("ordini.php");
              }else{
                header("location: home.php");
              }
            }else if($_GET["p"] === "8"){
              include("conferma_ordine.php");
            }else if($_GET["p"] === "9"){
              include("impostazioni.php");
            }
        ?>
        <footer>
          <p>2018 - Realizzato da Riccardo Signoracci.</p>
        </footer>
  </body>
</html>
