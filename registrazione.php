<?php
session_start();
include("db_Con.php");

if(!empty($_SESSION['email'])){
  header("location: home.php");
}

if(isset($_POST['email'])){
  if($_POST['password'] !== $_POST['passwordConferma']){
    header("location: registrazione.php?error=1");
  }else{
    $email = $_POST['email'];
    $nome = $_POST['nome'];
    $cognome = $_POST['cognome'];
    $password = sha1($_POST['password']);
    $query = "SELECT * FROM utenti WHERE utenti.email = '$email'";
    $result = $conn->query($query);

    if($result->num_rows==0){
        $query_insert = "INSERT INTO `utenti`(`email`, `nome`, `cognome`, `psw`, `fornitore`) VALUES ('".$email."','".$nome."','".$cognome."','".$password."', 0)";
        $result = $conn->query($query_insert);
        $_SESSION['email'] = $_POST['email'];
        $_SESSION['admin'] = 0;
        header("location: home.php");
    }else{
        header("location: registrazione.php?error=2");
    }
  }
}

?>

<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>uniEat</title>
  <link rel="stylesheet" type="text/css" title="stylesheet" href="style/style_user.css">
  <link href='http://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
</head>
<body class="notLogged">
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/regError.js"></script>
	<section>
		<header>
			<h1>Iscriviti subito!</h1>
			<h2>Iscriviti e accedi a uniEat</h2>
      <a href="index.php" id="login">oppure effettua il login...</a>
      <p id="errore"></P>
		</header>
		<form name="form_registrazione" method="post" action="#">
			<fieldset>

				<legend><span>1</span>Generalit&agrave;</legend>
				<ul>
					<li><label for="nome">Nome</label></li>
					<li><input id="nome" type="text" name="nome" required/></li>
					<li><label for="cognome">Cognome</label></li>
					<li><input id="cognome" type="text" name="cognome" required/></li>
				</ul>
			</fieldset>
			<fieldset>
				<legend><span>2</span>Email &amp; Password</legend>
				<ul>
					<li><label for="email">Email</label></li>
					<li><input id="email" type="email" name="email" required/></li>
					<li><label for="password">Password</label></li>
					<li><input id="password" type="password" name="password" required/></li>
					<li><label for="passwordConferma">Conferma password</label></li>
					<li><input id="passwordConferma" type="password" name="passwordConferma" required/></li>
				</ul>
			</fieldset>
      <input id="privacy" type="checkbox" name="privacy" required>
      <label for="privacy">Accetto i termini e le condizioni.</label><br>
			<input id= "accedi" type="submit" name="Submit" value="Iscriviti" />
			<input id="reset" type="reset" value="Reset" /><label for="reset">Reset</label>

  </form>
</section>
</body>
</html>
